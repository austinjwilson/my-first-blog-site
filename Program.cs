using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using MySql.Data.MySqlClient;
using Microsoft.AspNetCore;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using MyFirstBlogSite.Data;

namespace MyFirstBlogSite
{
    public class Program
    {
        // Main ...
        public static void Main(string[] args)
        {
            //CreateHostBuilder(args).Build().Run();
            //connectToDb();
            //InsertData();
            //PrintData();
            var host = CreateWebHostBuilder(args).Build();

            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<BlogContext>();
                    DbInitializer.Initialize(context);
                }
                catch (Exception ex)
                {
                    var logger = services.GetRequiredService<ILogger<Program>>();
                    logger.LogError(ex, "An error occurred while seeding the database.");
                }
            }

            host.Run();


        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
               .UseStartup<Startup>();
    }

        //public static void connectToDb()
        //{
            //MySqlConnection myBlog = new MySqlConnection("Server=Desktop-LS06AV8;" +
                                          //"Database=MyFirstBlogSite;" +
                                          //"Uid=root;" +
                                          //"Pwd=#1Stunnas;");
        }
        /*protected void SubmitArticle(EventArgs e, object sender)
        {
            MySqlConnection conn = new MySqlConnection("Server=Desktop-LS06AV8;" +
                                          "Database=MyFirstBlogSite;" +
                                          "Uid=root;" +
                                          "Pwd=#1Stunnas;");

            //String userText = Text1.Text;*/



        
    
        //private static void InsertData()
        //{
            //using (var context = new BlogDBContext())
            //{
                //context.Database.EnsureCreated();
                //context.BlogContext.Add(new Blog
                /*{
                    ID = 1,
                    Title = "My 1st Blog",
                    Author = " ",
                    Article = "Lorem Ipsum",
                    Date_Time = " ", 
                });
                context.SaveChanges();
            }
        }

        private static void PrintData()
        {
            using (var context = new BlogDBContext())
            {
                var blogtable = context.BlogTables;
                foreach (var blogcontext in blogtable)
                {
                    var data = new StringBuilder();
                    data.AppendLine($"ID: {blogcontext.ID}");
                    data.AppendLine($"Title: {blogcontext.Title}");
                    data.AppendLine($"Author: {blogcontext.Author}");
                    data.AppendLine($"Article: {blogcontext.Article}");
                    data.AppendLine($"DateTime: {blogcontext.Date_Time}");
                    Console.WriteLine(data.ToString());
                }
            }
        }*/

       
    



