﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MyFirstBlogSite.Models;
using MySql.Data.MySqlClient;
using System.Web;
namespace MyFirstBlogSite.Controllers
{
    public class HomeController : Controller
    {
        //private readonly ILogger<HomeController> _logger;

        /*public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }*/

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Article()
        {
            return View();
        }

        /*public IActionResult SubmitArticle()
        {
            //MySqlConnection conn = new MySqlConnection("server=localhost;database=mysqltest;uid=root;pwd=root");

            //MySqlCommand cmd = null;
            //string cmdString = "";
            //conn.Open();

            //cmdString = "insert into student values(" + textBox1.Text + ",'" + textBox2.Text + "'," + textBox3.Text + ",'" + textBox4.Text + "'," + textBox5.Text + ")";

            //cmd = new MySqlCommand(cmdString, conn);
            //cmd.ExecuteNonQuery();

            //conn.Close();
            
            return View();
        }

        public IActionResult username() 
        {
            return View();
        }*/

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        
    }
}
