﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
//using MyFirstBlogSite.Data;
using MyFirstBlogSite.Models;


namespace MyFirstBlogSite.Models
{
    public class Blog
    {
        //private BlogDBContext context;
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string Article { get; set; }

        public DateTime Date { get; set; }
    }
    /*public class BlogDBContext : DbContext
    {
        public BlogDBContext()
        { }
        public DbSet<Blog> BlogTables { get; set; }
        public object BlogTable { get; internal set; }
        public object BlogContext { get; internal set; }
    }*/
}
